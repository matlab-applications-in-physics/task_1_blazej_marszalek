% MATLAB 2020b
% name: Zadanie 7
% author: blazej_marszalek
% date: 13.11.2020
% version: v1.0



%1. 
h = 6.63*10^-34 %stała Plancka
x = h/(2*pi)

%2. (funkcja sind() zwraca wartość sinusa kąta danego w stopniach, funckja
%exp() zwraca wartość danej potęgi liczby e)
x = sind(45/exp(1))

%3 (funkcja double() zwraca zmienną typu double)
x = double(0x0098d6)/double(1.445*10^23)

%4.
x = sqrt(exp(1)-pi)

%5.
x = round(pi, 13)

%6. %25 grudnia 2000
dni = 6 + 19*365 + 4 + 313 % ilość dni do końca 2000 + pełne przeżyte lata + dni przestępne + dni w roku 2020
godziny = 24*dni + 12 % każdy dzień trwa 24 h + 12 h z dzisiejszego dnia

%7.
x = atan((exp(sqrt(7)/2-log(6.4*10^6/10^8)))/(double(0xaaff)))

%8.
x = 0.25 * 10^-6 * 6.02214076*10^23 %jeden mol substancji zawiera liczbę Avogadro atomów

%9
0,01 * 1000 % 1% wszytskich cząstek stanowi izotop 13C