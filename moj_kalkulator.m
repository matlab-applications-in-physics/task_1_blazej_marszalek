% MATLAB 2020b
% name: moj_kalkulator
% author: blazej_marszalek
% date: 13.11.2020
% version: v1.0

%1. 
h = 6.62607015e-34 %[J*s] Planck constant
v1 = h/(2*pi)%value 1

%2. 
%no need to convert degrees to radians; "sind" takes degrees
v2 = sind(45/exp(1)) %value 2

%3. converting hexadecimal nuber to a decimal one
x = hex2dec('0x0098d6')
v3 = x/1.445e23 %value 3

%4.
v4 = sqrt(exp(1)-pi)%value 4 is a complex number

%5.
%displaying pi number with 12 characters after comma and converting it to a
%string
formatSpec = '%.12f';
y = num2str(pi,formatSpec)
%finding the 12th character of pi after comma
v5 = y(14)
%displaying the 12th character of pi after comma
disp(v5)

%6. 
date1 = clock %current date
date2 = [2000 12 25 10 10 0] %date of my birth
a = etime(date1, date2) %gives number of seconds that have passed by from date1 untill date2
v6 = a/3600 %time a given in hours

%7.
R = 6,371e6 %[m] radius of Earth
n = hex2dec('0xaaff') %converting a hexadecimal nuber to a decimal
v7 = atan((exp(sqrt(7)/2-log(R/10^8)))/n)

%8.
A = 6.02214076e+23 %Avogadro number
v8 = 0.25*10^-3*A*9 %a single molecule of ethanol consists of 9 atoms

%9
nC = 0.25*10^-6*A*2 %number of atoms of Carbon in that mass of ethanol
nC13 = 0.01*nC %1% of these atoms are 13C
v9 = nC13/nC *1000 %number of promiles 13C in whole C